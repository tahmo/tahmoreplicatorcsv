# Import dependencies.
import TAHMO
from utility import columnMapping, getAggregationSettings
import schedule
import datetime
import dateutil
import time
import os
import csv
import numpy as np
from models import Base, Station
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# Load environment variables from file.
from dotenv import load_dotenv
load_dotenv()

# Initialize database session.
db_uri = os.getenv('SQLALCHEMY_DATABASE_URI')
engine = create_engine(os.getenv('DB_URI'))
Session = sessionmaker(bind=engine)

# Create all database tables.
Base.metadata.create_all(engine)

# Initialize TAHMO API.
api = TAHMO.apiWrapper()
api.setCredentials(os.getenv('TAHMO_API_USER'), os.getenv('TAHMO_API_PASSWORD'))


def updateMetadata():
    print('Update stations')
    session = Session()
    try:
        # Request station information from API.
        apiStations = api.getStations()

        # Create station objects from retrieved data.
        for stationCode in apiStations.keys():
            station = api.getStation(apiStations[stationCode]['id'])
            session.merge(Station(
                code=station['code'],
                name=station['location']['name'],
                locationid=station['location']['id'],
                countrycode=station['location']['countrycode'],
                latitude=station['location']['latitude'],
                longitude=station['location']['longitude'],
                elevationmsl=station['location']['elevationmsl'],
            ))
        session.commit()

        with open('data/stations.csv', 'w', newline='\n', encoding='utf-8') as stationFile:
            stationCSV = csv.writer(stationFile)
            stationCSV.writerow(['Code', 'Name', 'Latitude', 'Longitude', 'ElevationMSL'])
            for station in session.query(Station).all():
                stationCSV.writerow([station.code, station.name, station.latitude, station.longitude, station.elevationmsl])

    except Exception as e:
        print('Error while retrieving stations: %s' % str(e))
    finally:
        session.close()


def updateMeasurements():
    print('Update measurements')
    session = Session()
    stations = session.query(Station).all()
    session.close()
    for station in stations:
        updateMeasurementsForStation(station)


def updateMeasurementsForStation(station):
    print('Update measurements for station %s' % station.code)
    session = Session()
    try:
        if not os.path.exists('data/%s/' % (station.code)):
            os.makedirs('data/%s/' % (station.code))

        print('Last measurement', station.lastmeasurement)
        if (station.lastmeasurement):
            startDate = station.lastmeasurement + datetime.timedelta(0, 300)
        else:
            startDate = dateutil.parser.parse(os.getenv('START_TIME'))

        if os.getenv('SPLIT_FILES') == 'day':
            intervalPrefix = startDate.strftime('%Y%m%d')
            startDateInterval = dateutil.parser.parse("%sT00:00:00Z" % intervalPrefix)
            endDateInterval = startDateInterval + dateutil.relativedelta.relativedelta(days=+1) - datetime.timedelta(
                seconds=1)
        else:
            intervalPrefix = startDate.strftime('%Y%m')
            startDateInterval = dateutil.parser.parse("%s01T00:00:00Z" % intervalPrefix)
            endDateInterval = startDateInterval + dateutil.relativedelta.relativedelta(months=+1) - datetime.timedelta(
                seconds=1)

        df = api.getMeasurements(station.code, startDate=startDateInterval.strftime('%Y%m%d%H%M%S'), endDate=endDateInterval.strftime('%Y%m%d%H%M%S'))
        if len(df):
            # CSV export.
            dataframes = []

            if os.getenv('AGGREGATION_TEMPORAL') == 'hour':
                # Loffset to use the right bucket value for the GTS.
                df_hour = df.resample('1H', loffset='1h').agg(getAggregationSettings(list(df), '1H'))
                if(df_hour.tail(1).isnull().sum().sum() > 0):
                    df_hour.drop(df_hour.tail(1).index, inplace=True)
                dataframes.append(df_hour)

            dataframes.append(df)

            for index, df in enumerate(dataframes):
                df.index.name = 'timestamp'

                # Static columns.
                if os.getenv('FIXED_COLUMNS') == 'yes':
                    for expectedColumn in columnMapping.keys():
                        if expectedColumn not in df:
                            df[expectedColumn] = np.nan

                drop_columns = []
                columnNames = []
                for columnName in list(df):
                    if isinstance(columnName, tuple):
                        columnName, method = columnName

                    if columnName not in columnMapping:
                        drop_columns.append(columnName)
                    else:
                        if "unitMultiplier" in columnMapping[columnName]:
                            df[columnName] = df[columnName] * columnMapping[columnName]['unitMultiplier']

                        columnNames.append(columnMapping[columnName]['name'])

                # Drop unused columns.
                df.drop(columns=drop_columns, axis=1, inplace=True)
                df.columns = columnNames

                # Order columns alphabetically.
                df = df.reindex(columns=sorted(df.columns))

                # Save the dataframe to a CSV file.
                df.to_csv('data/%s/%s%s_%s.csv' % (station.code, station.code, '_hourly' if index == 0 and os.getenv('AGGREGATION_TEMPORAL') else '', intervalPrefix), na_rep=os.getenv('NA_VALUE'), date_format=os.getenv('DATE_FORMAT'))

                if os.getenv('CURRENT_FILE_ROWS') != '' and index == 0:
                    # Save last rows to the current file.
                    df.tail(int(os.getenv('CURRENT_FILE_ROWS'))).to_csv('data/%s/%s_current.csv' % (station.code, station.code),
                                        na_rep=os.getenv('NA_VALUE'), date_format=os.getenv('DATE_FORMAT'))

        # Three days margin is applied before we're going to skip ahead in case of no data.
        if len(df) or (datetime.datetime.now(endDateInterval.tzinfo) > endDateInterval + datetime.timedelta(3)):
            print('Updating last measurement data into database', (datetime.datetime.now(endDateInterval.tzinfo) > endDateInterval + datetime.timedelta(3)), endDateInterval if (datetime.datetime.now(endDateInterval.tzinfo) > endDateInterval + datetime.timedelta(3)) else datetime.datetime.fromtimestamp(df.tail(1).index.values.item() / 1e9, tz = endDateInterval.tzinfo))
            session.merge(Station(
                code=station.code,
                lastmeasurement=endDateInterval if (datetime.datetime.now(endDateInterval.tzinfo) > endDateInterval + datetime.timedelta(3)) else datetime.datetime.fromtimestamp(df.tail(1).index.values.item() / 1e9, tz = endDateInterval.tzinfo)
            ))
            session.commit()

    except Exception as e:
        print('Error while retrieving measurements for station %s: %s' % (station.code, str(e)))
    finally:
        print('Finished updating measurements for station %s' % station.code)
        session.close()


# Schedule periodic update jobs.
print('Scheduling jobs')
schedule.every(1).hour.at(":00").do(updateMetadata)
schedule.every(1).hour.at(":20").do(updateMeasurements)

# Manually trigger updates for testing purposes.
updateMetadata()
updateMeasurements()

while True:
    schedule.run_pending()
    time.sleep(1)
