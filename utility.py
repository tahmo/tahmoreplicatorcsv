import numpy as np
import math

columnMapping = {
    'ap': {
        'name': 'AtmosphericPressure',
        'unitMultiplier': 10,
        'units': 'hPa'
    },
    'wd': {
        'name': 'WindDirection',
        'units': 'degrees'
    },
    'ld': {
        'name': 'LightningDistance',
        'units': 'km'
    },
    'le': {
        'name': 'LightningEvents',
        'units': ''
    },
    'pr': {
        'name': 'Precip',
        'units': 'mm'
    },
    'wg': {
        'name': 'WindGusts',
        'units': 'm/s'
    },
    'ws': {
        'name': 'WindSpeed',
        'units': 'm/s'
    },
    'ra': {
        'name': 'GlobalRadiation',
        'units': 'W/m2'
    },
    'rh': {
        'name': 'RH',
        'unitMultiplier': 100,
        'units': '%'
    },
    'te': {
        'name': 'AirTemperature',
        'units': 'degrees C'
    }
}


# Custom function to aggregate wind direction.
def aggregateDirection(directions, minimumEntries=1):
    vector_x = list(map(lambda x: math.sin(x * math.pi / 180), filter(lambda v: v == v, directions)))
    vector_y = list(map(lambda x: math.cos(x * math.pi / 180), filter(lambda v: v == v, directions)))

    return None if (len(directions) < minimumEntries or np.count_nonzero(~np.isnan(directions)) < minimumEntries) else ((math.atan2(sum(vector_x) / len(vector_x), sum(vector_y) / len(vector_y)) * 180 / math.pi) + 360) % 360

def aggregateDirection1H(values):
    return aggregateDirection(values, 11)

# Custom function to take sum of measurements.
# Case - Less than minimumEntries of non-NaN data: return NaN.
# Else: return sum of non-Nan values.
def customSum(values, minimumEntries=1):
    return None if (len(values) < minimumEntries or np.count_nonzero(~np.isnan(values)) < minimumEntries) else np.nansum(values)

def customSum1H(values):
    return customSum(values, 11)

# Custom function to take average of measurements.
# Case - Less than minimumEntries of non-NaN data: return NaN.
# Else: return average of non-Nan values.
def customAverage(values, minimumEntries=1):
    return None if (len(values) < minimumEntries or np.count_nonzero(~np.isnan(values)) < minimumEntries) else np.nanmean(values)

def customAverage1H(values):
    return customAverage(values, 11)

# Custom function to take maximum of measurements.
# Case - Less than minimumEntries of non-NaN data: return NaN.
# Else: return maximum of non-Nan values.
def customMaximum(values, minimumEntries=1):
    return None if (len(values) < minimumEntries or np.count_nonzero(~np.isnan(values)) < minimumEntries) else np.nanmax(values)

def customMaximum1H(values):
    return customMaximum(values, 11)

def getAggregationSettings(columns, aggregationPeriod):
    aggregationSettings = {}

    if aggregationPeriod == '1H':
        # Scheme of variables that require non-mean aggregation.
        aggregationExceptions = {
            'le': [customSum1H],
            'pr': [customSum1H],
            'te': [customAverage1H],
            'wd': [aggregateDirection1H],
            'wg': [customMaximum1H]
        }
    else:
        # Scheme of variables that require non-mean aggregation.
        aggregationExceptions = {
            'le': [customSum],
            'pr': [customSum],
            'te': [customAverage],
            'cp': [customMaximum],
            'wd': [aggregateDirection],
            'wg': [customMaximum]
        }

    for columnName in columns:
        # Note: column name can include sensor code when a variable is measured by multiple sensors at the same time.
        variableCode = columnName.split('_')[0]

        if variableCode in aggregationExceptions:
            aggregationSettings[columnName] = aggregationExceptions[variableCode]
        else:
            if aggregationPeriod == '1H':
                aggregationSettings[columnName] = [customAverage1H]
            else:
                aggregationSettings[columnName] = [customAverage]

    return aggregationSettings